#!/usr/bin/env python

def my_xrange(start_index, last_index, step):
    n = start_index
    if (n<last_index)&(step<0) | (n>last_index)&(step>0):
        return 
    while (n - last_index)*(last_index - start_index) < 0:
        yield n
        n += step

        
print ('------')

for i in my_xrange(10, 20, 3):
    print (i)

print ('------')

for i in my_xrange(-10, 2, 3):
    print (i)

print ('------')

for i in my_xrange(1, -20, -3):
    print (i)

print ('------')

for i in my_xrange(-100, -20, -3):
    print (i)


print ('------')

for i in my_xrange(-2, 2, -1):
    print (i)