#!/usr/bin/env python
import sys, errno
from optparse import OptionParser


BLOCK_SIZE_DEFAULT = 1024  
DEBUG = False

class OptionError(Exception):
    pass    
class CatError(Exception):
    pass

def debug(msg):
    if (DEBUG):
        sys.stdout.write(msg)

def error(msg):
    sys.stderr.write("\n%s\n" % msg)


def cat_opt_parser(argv):
    
    error_msg = None    
    parser = OptionParser()
    parser.add_option("-b", "--block_size", action="store", dest="BLOCK_SIZE",
    help="specify buffer-size you want to read", default=BLOCK_SIZE_DEFAULT)  
    options, files = parser.parse_args()
    try:
        block = int(options.BLOCK_SIZE)
    except ValueError:
        error_msg = "Block size should be a number"        
    else:    
        if block <= 0:
            error_msg = "Block size can't be <= 0"
    if error_msg:
        raise OptionError(error_msg)
    return files, block
    
def cat(files, size):
    error_msg = None
    try:
        for f in files:      
            with open(f, 'rb') as of:                
                while True:
                    chunk = of.read(size)
                    if chunk:
                        sys.stdout.write(chunk)
                    else:
                        break
    except IOError, e:
        if e.errno == errno.ENOENT:           
            error_msg = "No such file or directory: {}".format(f)
        elif e.errno == errno.EACCES:
            error_msg = "Access error" 
    except MemoryError, e:  
        error_msg = "Memory Error"
    except OverflowError, e:  
        error_msg = "Your block size is too big"

    if error_msg:
        raise CatError(error_msg)
        

def main(*argv):
    rc = None
    e = ''
    try:
        cat(*cat_opt_parser(sys.argv[1:]))
        
    except OptionError, e:
        rc = 1
    except CatError, e:
        rc = 2
          
    if rc is not None and e:
        error(e)
    return rc

exit(main(*sys.argv))
       
