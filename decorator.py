#!/usr/bin/env python
import datetime, sys

DEBUG = False

def debug(msg):
    if (DEBUG):
        sys.stdout.write(msg)
        
def format_name_date(function):
    return [function.__name__, datetime.datetime.now()]
    
def log_decorator(func):
    f = format_name_date(func)
    
    def wrapper(some_list, number):
    
        print("Starting execution of {} at {}".format(*f))
               
        short_list = some_list[5:]
        flight_num = create_new_number(func(short_list, number)) 
        
        debug(str(flight_num))
        print "Your flight number is:", flight_num
        print("Ending execution of {} at {}".format(*f))
        
    def create_new_number(num):
    
        num = int(num)**2        
        num = "HD" + str(num)
        return num
        
    return wrapper
    
@log_decorator

def make_flight_number(flights_list, flight_number):

    flight_number = flights_list[0][2:]
    return flight_number
    
flights_list = ["HD331", "HD331", "HD331", "HD454", "HD566", "HD457"]
flight_number = 0
make_flight_number(flights_list, flight_number)
           
