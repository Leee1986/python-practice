#!/usr/bin/env python

def my_zip(*args):
	iters=map(iter, args)
	while iters:
		res =[next(i) for i in iters]
		print res
		yield tuple(res)



x = [1,2,7,8,0,3]

y = ["a", "b", "c", "g", "k"]


for tpl in my_zip(x,y):
 
	print tpl

print '----------'

for tpl in zip(x,y):

	print tpl


