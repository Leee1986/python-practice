#!/usr/bin/env python

def square_lst(x):
    return [i**2 for i in x]
        
lst = [1,2,3,4,0]
print square_lst(lst)

def second_elem(x):
    return [v for i,v in enumerate(x) if i%2 != 0]
    
lst1 = [1000,245,5,-34,32, 89,90]

print second_elem(lst1)

def second_elem_prime(x):
    return [i**2 for i in x[::2] if i%2 == 0]  

lst2 = [10,2,20,4,30,6,2,8]
print second_elem_prime(lst2)
